# TagLibIOS #

This project wraps TagLib 1.9.1 into an iOS framework.

## Licence ##

TagLib is released under the LGPL/MPL licences. See their website for more information: http://taglib.github.io/api/

Any additional sources / configurations in this framework are available under the `Beer Licence`:

    The user is allowed to do anything with the licensed material. Should the user of the product meet 
    the author and consider the software useful, he is encouraged to buy the author a beer.

## HOWTO ##

Steps to build the framework:

1. Open the project file in Xcode
2. Select a simulator build (say, iPhone 6)
3. Edit the "TagLibIOS" scheme and change the "Run" Build Configuration to `Release`
4. Hit `Product > Build` 
5. Your framework will be built into directory `~/Library/Developer/Xcode/DerivedData/TagLibIOS-<something>/Build/Products/Release-iphonesimulator/TagLibIOS.framework` 

## Code examples ##

Use **#import <TagLibIOS/TagLibIOS.h>** to include the headers into your project. 

TagLib API & simple code sample can be found here: http://taglib.github.io/api/